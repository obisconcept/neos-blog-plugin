# Neos Blog Plugin

Some introduction ...

## Installation

Add the package in your site package composer.json
```
"require": {
    "obisconcept/neos-gmaps-plugin": "~1.0"
}
```

Add these lines to the main Routes.yaml before Neos Neos subroutes definition:
```
-
  name: 'ObisConceptNeosBlogPlugin'
  uriPattern: '<ObisConceptNeosBlogPluginSubroutes>'
  subRoutes:
    ObisConceptNeosBlogPluginSubroutes:
      package: ObisConcept.NeosBlogPlugin
```

## Version History

### Changes in 1.0.0
- First version of the Neos CMS Blog plugin