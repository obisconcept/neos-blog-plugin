<?php namespace ObisConcept\NeosBlogPlugin\ViewHelpers\Widget;

use Neos\Flow\Annotations as Flow;
use Neos\ContentRepository\Domain\Model\NodeInterface;
use Neos\ContentRepository\ViewHelpers\Widget\PaginateViewHelper as ContentRepositoryPaginateViewHelper;

class PaginateViewHelper extends ContentRepositoryPaginateViewHelper
{

    /**
     * @Flow\Inject
     * @var \ObisConcept\NeosBlogPlugin\ViewHelpers\Widget\Controller\PaginateController
     */
    protected $controller;
}
